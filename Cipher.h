//Сipher.h - abstract class of ciphers
#pragma once
#ifndef CIPHER_H
#define CIPHER_H

#include <fstream>
#include <cstdint>
#include <random>
#include <cstdlib>
#include <string>

#define BLOCK_SIZE_DEFAULT 4

class Cipher{
protected:
	uint8_t block_size = BLOCK_SIZE_DEFAULT;
	uint8_t* temp_block;
	uint8_t* curr_block;
	uint8_t* prev_block;

	/* encrypt or decrypt mode */
	std::string mode;

	uint8_t** key;
	uint8_t raws_number = 1; // 1 is default value
	uint8_t columns_number = block_size;

	uint8_t* IV;
	virtual void encrypt(uint8_t*) = 0;
	virtual void decrypt(uint8_t*) = 0;

	void deep_copy(uint8_t*, uint8_t*);//dest, src
	void XOR(uint8_t*, uint8_t*);//block1(dest), block2
	void fill_array(size_t, size_t, uint8_t**);
	void fill_array(size_t, uint8_t*);
	uint32_t uint8t_arr_to_uint32t(uint8_t*);
public:
	Cipher(string, bool);//is matrix cipher?
	virtual ~Cipher(void);
	
	std::ofstream& crypto(std::ifstream&, std::ofstream&, string, string, string);
	
	/* encryption modes */
	void ECB(void);
	void CBC(void);
	void CFB(void);
	void OFB(void);
	
	/* padding modes */
	void PKCS7(uint8_t*, uint8_t);
	void ANSI_X923(uint8_t*, uint8_t);
	void ISO_10126(uint8_t*, uint8_t);

};

#endif