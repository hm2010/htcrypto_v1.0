//Vigenere.h
#pragma once
#ifndef VIGENERE_H
#define VIGENERE_H

#include "Cipher.h"
#define MAX_VALUE 255

class Vigenere: public Cipher{
private:
	void encrypt(uint8_t*);
	void decrypt(uint8_t*);
	
public:
	Vigenere(string);
	~Vigenere(void);
};

#endif