#include "Transposition.h"

Transposition::Transposition(string _mode): Cipher(_mode, false){}
Transposition::~Transposition(void){}

void Transposition::encrypt(uint8_t* block){
	uint8_t* result;
	result = new uint8_t[block_size];

	for (uint8_t i = 0; i < block_size; i++){
		result[key[0][i]] = block[i];
	}
	deep_copy(block, result);
}
void Transposition::decrypt(uint8_t* block){
	uint8_t* result;
	result = new uint8_t[block_size];

	for (uint8_t i = 0; i < block_size; i++){
		result[i] = block[key[0][i]];
	}
	deep_copy(block, result);	
}