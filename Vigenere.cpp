#include "Vigenere.h"

Vigenere::Vigenere(string _mode): Cipher(_mode, false){}

Vigenere::~Vigenere(void){}

void Vigenere::encrypt(uint8_t* block){
	for (uint8_t i = 0; i < block_size; i++){
		block[i] = (block[i] + key[0][i]);
	}
}
void Vigenere::decrypt(uint8_t* block){
	for (uint8_t i = 0; i < block_size; i++){
		block[i] = (block[i] - key[0][i] + MAX_VALUE);
	}
}