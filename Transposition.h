//Transposition.h
#pragma once
#ifndef TRANSPOSITION_H
#define TRANSPOSITION_H

class Transposition: public Cipher{
private:
	void encrypt(uint8_t*);
	void decrypt(uint8_t*);
public:
	Transposition(string);
	~Transposition(void);
	
};

#endif