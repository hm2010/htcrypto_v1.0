//Hill.h
#pragma once
#ifndef HILL_H
#define HILL_H

#include "Cipher.h"
#define MAX_VALUE 255

class Hill: public Cipher{
private:
	void encrypt(uint8_t*);
	void decrypt(uint8_t*);
public:
	Hill(string);
	~Hill(void);
};

#endif