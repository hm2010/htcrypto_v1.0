#include "Cipher.h"

void Cipher::fill_array(size_t raws_num, size_t columns_num, uint8_t** dest){
	for (uint8_t i = 0; i < raws_num; i++){
		for (uint8_t j = 0; j < columns_num; j++){
			std::cin >> dest[i][j];
		}
	}
}
void Cipher::fill_array(size_t columns_num, uint8_t* dest){
	for (uint8_t i = 0; i < columns_num; i++){
		std::cin >> dest[i];
	}
}
void Cipher::deep_copy(uint8_t* dest, uint8_t* src){
		for (uint8_t i = 0; i < block_size; i++){
			dest[i] = src[i];
		}
}

void Cipher::XOR(uint8_t* block1, uint8_t* block2){
	/* result in block1 */
	for (uint8_t i = 0; i < block_size; i++){
		block1[i] ^= block2[i];
	}	
}

uint32_t uint8t_arr_to_uint32t(uint8_t* block){
	uint32_t result = 0;
	uint32_t temp = 1;
	for (uint8_t i = 0; i < block_size; i++){
		result += (temp * block[i]);
		temp *= 2;
	}
	return result;
}

Cipher::Cipher(string _mode, bool is_matrix_cipher){
	if (is_matrix_cipher){
		raws_number = block_size;
	}

	mode = _mode;

	curr_block = new uint8_t[block_size];
	prev_block = new uint8_t[block_size];
	temp_block = new uint8_t[block_size];

	key = new uint8_t*[raws_number];
	for (uint8_t i = 0; i < raws_number; i++){
		key[i] = new uint8_t[columns_number];
	}
	fill_array(raws_number, columns_number, key);

	IV = new uint8_t[block_size];
	fill_array(block_size,IV);
}

Cipher::~Cipher(void){
	for (uint8_t i = 0; i < raws_number; i++){
		delete[] key[i];
	}
	delete[] key;

	delete[] IV;
}

std::ofstream& Cipher::crypto(std::ifstream& file_in, string enc_mode, string pad_mode){
	deep_copy(prev_block, IV);

	uint8_t not_filled;

	while (!file_in.eof()){
		file_in.getline(curr_block, block_size);
		if (mode == "encrypt"){
			not_filled = file_in.gcount();
			if (file_in.gcount() < block_size){
				if (pad_mode == "PKCS7"){
					PKCS7(curr_block, not_filled);
				}
				else if (pad_mode == "ANSI_X923"){
					ANSI_X923(curr_block, not_filled);
				}
				else if (pad_mode == "ISO_10126"){
					ISO_10126(curr_block, not_filled);
				}
			}
		}

		if (enc_mode == "ECB"){
			ECB(void);
		}
		else if (enc_mode == "CBC"){
			CBC(void);
		}
		else if(enc_mode == "CFB"){
			CFB(void);
		}
		else if(enc_mode == "OFB"){
			OFB(void);
		}
	}
	if (mode == "decrypt"){
		not_filled = curr_block[block_size - 1];
		if (pad_mode == "PKCS7"){
			PKCS7(curr_block, not_filled);
		}
		else if (pad_mode == "ANSI_X923"){
			ANSI_X923(curr_block, not_filled);
		}
		else if (pad_mode == "ISO_10126"){
				ISO_10126(curr_block, not_filled);
		}
	}

}


void Cipher::ECB(void){
	if (mode == "encrypt"){
		encrypt(curr_block);
	}
	else{
		decrypt(curr_block);
	}	
}

void Cipher::CBC(void){
	if (mode == "encrypt"){
		XOR(curr_block, prev_block);
		encrypt(curr_block);
		deep_copy(prev_block, curr_block);

	}
	else{
		deep_copy(temp_block, curr_block);
		decrypt(curr_block);
		XOR(curr_block, prev_block);
		deep_copy(prev_block, temp_block);
	}
}

void Cipher::CFB(void){
	encrypt(prev_block);
	XOR(curr_block, prev_block);
	deep_copy(prev_block, curr_block);
}

void Cipher::OFB(void){
	encrypt(prev_block);
	XOR(curr_block, prev_block);	
}

void Cipher::PKCS7(uint8_t*, uint8_t added){
	if (mode == "encrypt"){
		for (uint8_t i = 0; i < added; i++){
			block[block_size - 1 - i] = added;
		}
	}
	else{
		for (uint8_t i = 0; i < added; i++){
			block[block_size - 1 - i] = 0;
		}
	}
} 
void Cipher::ANSI_X923(uint8_t* block, uint8_t added){
	if (mode == "encrypt"){
		block[block_size - 1] = added; 
	}
	else{
		block[block_size - 1] = 0;
	}
}
void Cipher::ISO_10126(uint8_t*, uint8_t){
	if (mode == "encrypt"){ 
		srand(uint8t_arr_to_uint32t(IV));
		block[block_size - 1] = added;
		for (uint8_t i = 1; i < added; i++){
			block[block_size - 1] = rand();
		}
	}
	else{
		for (uint8_t i = 0; i < added; i++){
			block[block_size - 1 - i] = 0;
		}
	}
}